import IconHouse from './iconHouse.svg'
import IconSekolah from './iconSekolah.svg'
import IconSholat from './iconSholat.svg'
import IconTutor from './iconTutor.svg'
import IconProfile from './iconProfil.svg'
import IconSchoolType from './SchoolType.svg'
import IconClassification from './Classification.svg'
import IconMap from './iconMap.svg'
import IconFineTutor from './iconFinetutor.svg'
import IconGradeTutor from './iconGradeTutor.svg'

export{
    IconHouse,
    IconSekolah,
    IconSholat,
    IconTutor, 
    IconProfile, 
    IconSchoolType,
    IconClassification,
    IconMap,
    IconFineTutor,
    IconGradeTutor}