import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import {IconHouse,IconSekolah,IconSholat,IconTutor} from '../../aseet'
import {WARNA_UTAMA,WARNA_DISABLE} from '../../Util/constan'

const TabItem = ({ isFocused,onPress,onLongPress, label}) => {
    const Icon =() =>{
        if(label==="Home")return isFocused ? <IconHouse/> : <IconHouse/>
        if(label==="Sekolah")return isFocused ? <IconSekolah/> : <IconSekolah/>
        if(label==="Sholat")return isFocused ? <IconSholat/> :<IconSholat/>
        if(label==="Tutor")return isFocused ? <IconTutor/> : <IconTutor/>
        
        return<IconHouse/>
    }
    return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.container}>
            <Icon/>
            <Text style={styles.text(isFocused)}> {label}</Text>
        </TouchableOpacity>
    );
};

export default TabItem

const styles = StyleSheet.create({
    container:{
        alignItems:'center'
    },
    text: (isFocused) => ({
        fontSize:13,
        color: isFocused? WARNA_UTAMA : WARNA_DISABLE,
        marginTop:8
    })
})
