import SplashBackground from './SplashBackground.png'
import ImageHeader from './Header.png'
import ImageProfile from './Profile.png'
import ImageHeaderTemplate from './HeaderTemplate.png'
import ImageSekolah from './Sekolah.png'
export{
    SplashBackground,
    ImageHeader,
    ImageProfile, 
    ImageHeaderTemplate, 
    ImageSekolah}