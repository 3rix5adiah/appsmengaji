import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Button,
    Alert
} from 'react-native'
import { IconFineTutor, IconGradeTutor, ImageSekolah } from '../../aseet'


const Tutor = () => {
    return (
        <View style={styles.page}>
            <ImageBackground source={ImageSekolah}
                style={styles.imgsekolah}>
                <Text style={styles.fines}>Find Tutor?</Text>
                <TouchableOpacity style={styles.finetutor}>
                    <IconFineTutor />
                </TouchableOpacity>
                <TouchableOpacity style={styles.gradetutor}>
                    <IconGradeTutor />
                </TouchableOpacity>
                <View style={styles.container}>
                    <Text style={styles.fines}>Sesion</Text>
                </View>
            </ImageBackground>
            <View style={styles.container1}>
                <Button               
                  title="Ok"onPress={() => Alert.alert('Simple Button pressed')} />
            </View>
        </View>
    )
}

export default Tutor
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    imgsekolah: {
        width: windowWidth,
        height: windowHeight * 0.25
    },
    fines: {
        fontSize: 20,
        fontFamily: 'TitilliumWeb-Regular'
    },
    finestutortext: {
        fontSize: 20,
        fontFamily: 'TitilliumWeb-Regular',
        alignItems: 'center'
    },
    finetutor: {
        backgroundColor: 'white',
        padding: 17,
        margin: 10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    gradetutor: {
        backgroundColor: 'white',
        padding: 17,
        margin: 10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    container: {
        backgroundColor: 'white',
        padding: 17,
        margin: 10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    container1: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 30,
    },
})
