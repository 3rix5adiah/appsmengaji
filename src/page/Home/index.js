import React from 'react'
import {
    StyleSheet, 
    Text, 
    View, 
    ImageBackground,
    Dimensions,
    TouchableOpacity,
    Image
        }
    from 'react-native'
import {
     IconProfile,
     ImageHeader,
     ImageHeaderTemplate } from '../../aseet'
import Artikel from '../../compone/artikel'


const Home = () => {
    const Icon = () => {
        return <IconProfile />
    }


    return (
        <View style={styles.page}>
            <ImageBackground
                source={ImageHeaderTemplate}
                style={styles.headerTemplate}>
                <TouchableOpacity style={styles.profile}>
                    <Icon />
                </TouchableOpacity>
                    <Image source={ImageHeader}
                    style={styles.header}>                  
                 </Image>
            </ImageBackground>
            <View>
                <Text>
                </Text>
                <Text style={styles.artikel}>
                    Artikel
                </Text>
            </View>
            <Artikel />
            <View>
                <Text style={styles.artikel2}>
                </Text>
            </View>
            <Artikel />
        </View>

    )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    headerTemplate: {
        width: windowWidth,
        height: windowHeight * 0.25
    },
    header: {
        width: 355,
        height: 170,
        marginLeft: 15
    },
    artikel:{
        padding: 20
    },
    profile: {
        marginLeft: 335
    }
})
