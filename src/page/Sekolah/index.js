import React from 'react'
import { StyleSheet, Text, View, ImageBackground, Image, Dimensions, TouchableOpacity } from 'react-native'
import { color } from 'react-native-reanimated'
import { IconClassification, IconMap, IconSchoolType, ImageSekolah } from '../../aseet'

const Sekolah = () => {
    const Icon = () => {
        return <IconSchoolType />
    }
    return (
        <View style={styles.page}>
            <ImageBackground source={ImageSekolah}
                style={styles.imgsekolah}>
                <View style={styles.fineschool}>
                <Text style={styles.fines}>Find School?</Text>
                    <TouchableOpacity style={styles.searchschool}>
                        <Icon />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.classification}>
                        <IconClassification />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.map}>
                        <IconMap />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
            <Text></Text>
        </View>
    )
}

export default Sekolah
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    page: {
        flex: 1
    },
    imgsekolah: {
        width: windowWidth,
        height: windowHeight * 0.25
    },
    fineschool: {
        marginTop: windowHeight * 0.08,
        fontSize: 30
    },
    fines: {
        fontSize: 20
    },
    searchschool: {
        backgroundColor: 'white',
        padding: 17,
        margin:10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    classification: {
        backgroundColor: 'white',
        padding: 17,
        margin:10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    },
    map: {
        backgroundColor: 'white',
        padding: 17,
        margin:10,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,

        elevation: 9,
    }

})
