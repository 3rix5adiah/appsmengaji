import Home from './Home'
import Sekolah from './Sekolah'
import Sholat from './Sholat'
import Tutor from './Tutor'
import Splash from './Splash'

export{Home, Sekolah,Sholat,Tutor, Splash}
